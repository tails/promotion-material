What Tails is
-------------

[slide1]
[boyska speaks]

Hi,

My name is boyska - and I'm intrigeri - and we work for Tails.
Thanks a lot to Tor for inviting us.

[intrigeri speaks]

Tails is a portable operating system that protects against surveillance and censorship.

Instead of starting your computer on Windows or macOS, you can start it on your Tails USB stick whenever you need extra security.

And when you shutdown your computer, Tails leaves no trace of what you did on the computer.

Also, everything you do from Tails on the Internet goes through the Tor network.


Tails is developed with security in mind; but we know that making it easy to use is as important as cryptography details:

if Tails is not easy to use, people will use tools that are less safe.

So improving usability has always been, and continue to be, a big focus for us.

Today we will present you two of the most recent innovations in Tails.

Topic 1: Tor Connection
-----------------------

[boyska speaks]

[switch to slide about censorship and bridges]

As you know, unrestricted access to the Internet cannot be taken for granted. Many people all around the world suffer from censorship and surveillance.
An important functionality for those people is the use of Tor Bridges. Bridges are a great mechanism to avoid censorship and surveillance. However, they are not always easy to use.

[go to slide which plays qrcode video]

In Tails 5.8 we will introduce QR code scanning: this will make the use of bridges drammatically easier. In fact, you'll now be able to receive the QR code on your smartphone, show it in front on the webcam, and just connect to the Tor network.

[(automatically) go to slide with Tor Connection success screen]

Topic 2: Persistent Storage
---------------------------

[slides 5: screenshot "insert password"]
[intri speaks]


Some journalists use Tails as a safe work environment and storage area for sensitive material, thanks to our Persistent Storage feature. But we learnt from users that they found it too complicated to use, and many fell back to other, less safe solutions. To fix this, we have to make Tails easier to use. That's why we're glad to present The new Persistent Storage, which makes it easier and quicker to set up:

[slide 6: screenshot of the "configure features"]

- The new Persistent Storage doesn't require restarting after creating it or each time you activate a new feature.

- It will also make it easier for us to persist more settings in the future.

- And finally, it's looks so much better!


Donate
------

[boyska speaks]
[slide donate]


Journalists and whistleblowers use Tails to denounce the wrongdoings of governments and corporations.
Activists use Tails to avoid surveillance and organize their struggles for liberatory social change.
Domestic violence survivors use Tails to escape surveillance at home.
Privacy-concerned citizens use Tails to avoid online tracking and censorship.

Donate now to help us give those people better tools.

Donations from passionate people like you are our most valuable source of funding because they guarantee our independence.

Thanks to Tor! Tails would not exist without the foundations that Tor provides.

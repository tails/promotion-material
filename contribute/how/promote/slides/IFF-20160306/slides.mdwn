<!--

- Who already knows what Tails is about?
- Who has used Tails in the past?

-->

What is Tails
=============

- Operating system on a USB stick designed for privacy and anonymity
- Safe environment for:
  <ul>
    <li>Anonymous Internet and censorship circumvention (all networking goes through Tor)</li>
    <li>Working on sensitive documents (leaves no trace on the computer)</li>
    <li>Using privacy tools preconfigured in Tails (OTR, Enigmail, encrypted partitions, etc.)</li>
  </ul>

What is Tails
=============

- Operating system on a USB stick designed for privacy and anonymity
- Safe environment for:
  <ul>
    <li>Anonymous Internet and censorship circumvention (all networking goes through Tor)</li>
    <li>Working on sensitive documents (leaves no trace on the computer)</li>
    <li>Using privacy tools preconfigured in Tails (OTR, Enigmail, encrypted partitions, etc.)</li>
  </ul>
- 19000 users daily

What brought us here
====================

- UX testing at NUMA in Paris
  <ul>
    <li>Problems ended being somewhere else</li>
    <li>No feedback on Tor connection progress</li>
    <li>People could not connect through the captive portal at NUMA</li>
  </ul>

- Redesign work on Tor Launcher, the Tor configuration tool of Tor Browser
  <ul>
    <li><https://trac.torproject.org/projects/tor/wiki/doc/TorLauncherUX2016></li>
    <li>But in Tails we have to help connecting to the local network first</li>
  </ul>

<!--

Differences between Tails and Tor Browser
=========================================

- Tails needs to help you connect to the local network.
- Tails has more parameters to handle:
  - MAC spoofing
  - Time synchronization
  - Captive portal
- Tails has no access to Internet outside of Tor.
- Tails has no prior knowledge of proxy settings.
- Tor Browser is not aware on which local network you are connected to.
  Tails knows your local network (and you could save Tor settings accordingly).

-->

What we are trying to solve
===========================

<ul class="incremental">
  <li>No feedback when behind a captive portal</li>
  <li>No feedback when Tor is blocked or censored (and you need a bridge)</li>
  <li>No feedback on whether the connection to Tor is making progress</li>
  <li>Might require actions in very different places</li>
  <li>Allow saving settings for a given network (and reusing the same Tor entry guard)</li>
  <li>Make it easier to circumvent censorship</li>
</ul>

Let's do an experiment!
=======================

<ul class="incremental">
  <li>Any kind of questions: what? why?</li>
  <li>Things that you don't understand or doubts</li>
  <li>Suggestions, new ideas, etc.</li>
  <li>Use cases in which the proposal wouldn't work</li>
  <li>Privacy or technical issues behind the proposal</li>
  <li>Missing things</li>
</ul>

<!--

- FOLLOW: I'll show you a set of screens that follows a user path. Artificial
  because I get to choose.
- STUPID: You can write anything and there are no stupid questions, I'm testing
  my mock-ups here and not you.
- IDEAL: It's an experiment to explore what could be the ideal user experience
  but some on this stuff might be very hard to implement for us.
- INCOMPLETE: It's also not covering everything that can go wrong as we
  wouldn't have time to do so.

User decisions
==============

- Offline mode
- MAC spoofing
- Local network (Wi-Fi or wired)
- Wi-Fi password
- Probing or not probing
- Captive portal login
- Local proxy
- Tor bridges
- Save settings in persistence

Things that can go wrong
========================

- MAC spoofing can fail on some devices
- Internal clock is wrong and prevents connecting to Tor
- Direct connection to Tor can fail
- Various bridges and pluggable transports for Tor can fail
- Proxy configuration can be wrong
- Internet access can be blocked by a captive portal

Time
====

- Time before the release. You must set the time.
- Certificate expired. Please check the time.
- Consensus expired. Please check the time.

-->
